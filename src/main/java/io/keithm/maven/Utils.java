package io.keithm.maven;

import java.util.ArrayList;
import java.util.List;

public class Utils {

  public static <T> T[] sliceArray(T[] arr, int start, int end) {
    int size = end - start;
    List<T> l = new ArrayList<>(size);
    //Copying the contents of the array
    for (int i = 0; i < size; i++) {
      l.add(arr[start + i]);
    }
    return toArray(l);
  }

  public static <T> T[] toArray(List<T> list) {
    T[] array = (T[]) new Object[list.size()];
    for (int i = 0; i < list.size(); i++) {
      array[i] = list.get(i);
    }
    return array;
  }

  public static void sleep(final long time) {
    try {
      Thread.sleep(time);
    } catch (Exception e) {
      // No-op
    }
  }
}
