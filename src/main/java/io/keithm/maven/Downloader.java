package io.keithm.maven;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.utils.URIBuilder;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.CompletableFuture;

import static io.keithm.maven.Utils.toArray;

@Slf4j
@Singleton
public class Downloader {

  private static final String ROOT_PATH = "/maven";
  private static final String TEMP_PATH = "/temp";
  private static final List<String> MAVEN_URLS = Arrays.asList(
      "https://arti.keithm.io/artifactory/mvn/"
      // "https://repo.maven.apache.org/maven2",
      // "https://jcenter.bintray.com",
      // "https://maven-central.storage.googleapis.com/maven2/"
  );

  @Inject
  Downloader() {
  }

  public CompletableFuture<DownloadResult> download(String path) throws MalformedURLException, URISyntaxException {
    return downloadFromAll(path);
  }

  public DownloadResult exists(String path) throws FileNotFoundException {
    final File file = new File(Paths.get(ROOT_PATH, path).toAbsolutePath().toString());
    final boolean exists = file.exists();
    if (exists) {
      return DownloadResult.builder()
          .success(true)
          .filePath(file.getAbsolutePath())
          .error(Optional.empty())
          .build();
    } else {
      return DownloadResult.builder()
          .success(false)
          .filePath(null)
          .error(Optional.empty())
          .build();
    }
  }

  private static CompletableFuture<DownloadResult> download(final String url, final String filePath) {
    log.info("Downloading {} to {}", url, filePath);
    return CompletableFuture.supplyAsync(() -> {
      createFile(filePath);
      try (BufferedInputStream in = new BufferedInputStream(new URL(url).openStream());
           FileOutputStream fileOutputStream = new FileOutputStream(filePath)) {
        byte[] dataBuffer = new byte[1024];
        final StringBuilder bytes = new StringBuilder();
        int bytesRead = 0;
        while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
          fileOutputStream.write(dataBuffer, 0, bytesRead);
          for (byte b : dataBuffer) {
            bytes.append(b);
          }
        }
        return DownloadResult.builder()
            .success(true)
            .bytes(bytes.toString())
            .error(Optional.empty())
            .filePath(filePath)
            .build();
      } catch (FileNotFoundException notFoundException) {
        return DownloadResult.builder()
            .success(false)
            .error(Optional.of(notFoundException))
            .build();
      } catch (Exception e) {
        log.warn("Couldn't Download File \"{}\"", url, e);
        return DownloadResult.builder()
            .success(false)
            .error(Optional.of(e))
            .build();
      }
    });
  }

  public CompletableFuture<DownloadResult> downloadFromAll(final String path) {
    final List<CompletableFuture<DownloadResult>> futures = new ArrayList<>();

    for (String mavenRepo : MAVEN_URLS) {
      final String url = join(mavenRepo, path);
      futures.add(download(url,
          Paths.get(TEMP_PATH,
              Base64.getEncoder().encodeToString(url.getBytes()),
              path)
              .toAbsolutePath().toString()));
      if (!path.endsWith("sha1")) {
        final String shaUrl = url + ".sha1";
        download(shaUrl,
            Paths.get(TEMP_PATH,
                Base64.getEncoder().encodeToString(url.getBytes()),
                path)
                .toAbsolutePath().toString() + ".sha1")
            .handleAsync((result, err) -> {
              if (result != null && result.isSuccess()) {
                log.info("Downloaded SHA file {}", shaUrl);
              }
              return null;
            });
      }
    }

    CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]));

    for (CompletableFuture<DownloadResult> future : futures) {
      try {
        final DownloadResult result = future.get();
        if (result.isSuccess()) {
          return CompletableFuture.supplyAsync(() -> {
            final String newPath = Paths.get(ROOT_PATH, path)
                .toAbsolutePath().toString();
            createFile(newPath);
            moveFile(result.getFilePath(), newPath);
            moveFile(result.getFilePath() + ".sha1", newPath + ".sha1", true);
            return DownloadResult.builder()
                .success(true)
                .bytes(result.getBytes())
                .filePath(newPath)
                .error(Optional.empty())
                .build();
          });
        }
      } catch (Exception e) {
        log.warn("Download Failed", e);
      }
    }
    return CompletableFuture.failedFuture(new FileNotFoundException("None of the Downloads worked!"));
  }

  private static void createFile(final String path) {
    new File(new File(path).getParent()).mkdirs();
  }

  private void moveFile(final String path, final String newPath) {
    moveFile(path, newPath, false);
  }

  private void moveFile(final String path, final String newPath, final boolean quiet) {
    try {
      Files.move(Paths.get(path), Paths.get(newPath));
    } catch (IOException e) {
      if (!quiet) {
        log.warn("Couldn't Move File", e);
      }
    }
  }

  private static String join(final String base, final String path) {
    URIBuilder uriBuilder = null;
    try {
      uriBuilder = new URIBuilder(base);
      URI uri = uriBuilder.setPath(uriBuilder.getPath() + path)
          .build()
          .normalize();
      return uri.toString();
    } catch (Exception e) {
      return base + path;
    }
  }

  @Data
  @Builder
  public static class DownloadResult {
    private boolean success;
    private Optional<Throwable> error;
    private String filePath;
    private String bytes;
  }
}
