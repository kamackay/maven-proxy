package io.keithm.maven;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import io.javalin.Javalin;
import io.javalin.core.compression.Gzip;
import io.javalin.core.util.RouteOverviewPlugin;
import io.javalin.plugin.json.JavalinJson;
import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import static io.javalin.apibuilder.ApiBuilder.get;

@Slf4j
public class Server {
  private final Javalin app;
  private final Gson gson = new GsonBuilder()
      //.setPrettyPrinting()
      .create();
  private final int port = Optional.ofNullable(System.getenv("PORT"))
      .map(Integer::parseInt)
      .orElse(5956);
  private static final long BAD_REQUEST_LIMIT = 10000L;
  private final AtomicLong lastBadRequest = new AtomicLong(0);

  @Inject
  Server(final Downloader downloader) {
    this.app = Javalin.create(config -> {
      config.enableCorsForAllOrigins();
      config.requestLogger((ctx, time) -> {
        if (time > BAD_REQUEST_LIMIT) {
          log.error("Bad Request Time. Reporting.");
          this.lastBadRequest.set(System.currentTimeMillis());
        }
      });
      JavalinJson.setToJsonMapper(this.gson::toJson);
      JavalinJson.setFromJsonMapper(this.gson::fromJson);
      config.registerPlugin(new RouteOverviewPlugin("overview"));
      config.compressionStrategy(null, new Gzip(7));
    }).routes(() -> {
      get("/*", ctx -> {
        final Downloader.DownloadResult existsResult = downloader.exists(ctx.path());
        if (existsResult.isSuccess()) {
          try {
            try (final FileInputStream stream = new FileInputStream(existsResult.getFilePath())) {
              ctx.result(stream.readAllBytes());
              log.info("Cached Request for {}", ctx.path());
            }
          } catch (Exception e) {
            log.error("Couldn't Send Data", e);
          }
        } else {
          downloader.download(ctx.path())
              .handle((result, ex) -> {
                if (ex != null) {
                  if (ex instanceof FileNotFoundException) {
                    ctx.status(404).result("File Not Found");
                  } else {
                    ctx.status(500).result("Internal Proxy Error");
                  }
                } else {
                  if (result.isSuccess()) {
                    if (result.getBytes() != null) {
                      ctx.result(result.getBytes());
                      log.debug("Successfully passed Result Via Stream");
                      return null;
                    }

                    Utils.sleep(100);
                    try {
                      try (final FileInputStream stream = new FileInputStream(result.getFilePath())) {
                        ctx.result(stream.readAllBytes());
                      }
                    } catch (Exception e) {
                      log.error("Couldn't Send Data", e);
                    }
                  } else {
                    ctx.status(404).result("Not Found");
                  }
                }
                return null;
              });
        }
      });
    });
  }

  void start() {
    log.info("Starting Server...");
    this.app
        .start(port);
  }

}
