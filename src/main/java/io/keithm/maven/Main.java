package io.keithm.maven;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.slf4j.Logger;

import static io.keithm.domn8.util.Utils.humanizeBytes;
import static org.slf4j.LoggerFactory.getLogger;

public class Main {
  private static final Logger log = getLogger(Main.class);

  public static void main(String[] args) {
    try {
      final Runtime runtime = Runtime.getRuntime();
      log.info("CPU Cores Available: {}", runtime.availableProcessors());
      log.info("Memory Limit: {}", humanizeBytes(runtime.maxMemory()));
      final Injector injector = Guice.createInjector(new ServerModule());
      injector.getInstance(Server.class).start();
    } catch (Exception e) {
      getLogger(Main.class).error("Error Running API!", e);
    }
  }
}
