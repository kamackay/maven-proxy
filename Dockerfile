FROM gradle:jdk11 as builder

WORKDIR /app

COPY build.gradle ./

RUN gradle getDeps

COPY ./ ./

RUN gradle makeJar && \
    cp build/libs/*-all-*.jar ./proxy.jar

FROM registry.access.redhat.com/ubi8:latest

RUN yum update -y && yum install -y java-11-openjdk

WORKDIR /app/

COPY --from=builder /app/proxy.jar ./

CMD [ "java", "-jar", "proxy.jar" ]
