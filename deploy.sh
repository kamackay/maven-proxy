IMAGE=$1

time docker build . -t "$IMAGE" && \
    # docker-squash "$IMAGE" --tag "$IMAGE" && \
    docker push "$IMAGE" && \
    kubectl -n maven \
      set image statefulset/maven maven-proxy=$IMAGE && \
    kubectl -n maven rollout restart statefulset maven

sleep 5
ATTEMPTS=0
ROLLOUT_STATUS_CMD="kubectl -n maven rollout status statefulset/maven"
until $ROLLOUT_STATUS_CMD || [ $ATTEMPTS -eq 60 ]; do
  $ROLLOUT_STATUS_CMD
  ATTEMPTS=$((ATTEMPTS + 1))
  sleep 1
done

ECHO "Successfully deployed" $1